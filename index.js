'use strict';

const parseExpression = require("./parse-expression");

function findAll(regex, target) {
	let results = [], match;
	
	while (match = regex.exec(target)) {
		results.push(match);
	}
	
	return results;
}

module.exports = function(testcase) {
	let result;

	let [_, parent, child, initialExpression] = /var s,t,o,p,b,r,e,a,k,i,n,g,f,\s*([a-zA-Z]+)={"([a-zA-Z]+)":([^}]+)};/.exec(testcase);
	
	let modifyingExpressions = findAll(new RegExp(`${parent}\.${child}\s*([*+-])=\s*([^;]+)`, "g"), testcase).map((match) => {
		return {
			operation: match[1],
			expression: match[2]
		}
	}).map(({operation, expression}) => {
		return {
			operation,
			expression: parseExpression(expression)
		}
	});
	
	initialExpression = parseExpression(initialExpression);
	
	return {parent, child, initialExpression, modifyingExpressions};
}